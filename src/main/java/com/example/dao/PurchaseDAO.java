package com.example.dao;

import com.example.entities.Customer;
import com.example.entities.Purchase;
import com.example.entities.StockItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by admin on 16/03/2017.
 */
public interface PurchaseDAO extends JpaRepository<Purchase,Integer> {

    Purchase findById(int id);

    List<Purchase> findByStockItem(StockItem stockItem);

    List<Purchase> findByCustomer(Customer customer);


//    List<Purchase> openPurchases(Customer customer);


}
