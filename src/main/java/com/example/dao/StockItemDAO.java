package com.example.dao;

import com.example.entities.StockItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 14/03/2017.
 */
public interface StockItemDAO extends JpaRepository<StockItem,Integer> {

    StockItem findById(int id);

    StockItem findByTitle(String s);

    List<StockItem> findByManufacturer(String m);

    List<StockItem> findByPrice(double d);

    List<StockItem> findByCategory(String s);

}
