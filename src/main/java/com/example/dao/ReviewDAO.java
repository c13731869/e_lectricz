package com.example.dao;

import com.example.entities.Customer;
import com.example.entities.Review;
import com.example.entities.StockItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by admin on 16/03/2017.
 */
public interface ReviewDAO extends JpaRepository<Review,Integer> {

    Review findById(int id);

    List<Review> findByCustomer(Customer customer);

    List<Review> findByStockItem(StockItem stockItem);


}
