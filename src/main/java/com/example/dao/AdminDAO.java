package com.example.dao;

import com.example.entities.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by admin on 14/03/2017.
 */
public interface AdminDAO extends JpaRepository<Admin,Integer> {

    Admin findById(int id);
    Admin findAdminByEmailAndPassword(String email, String password);
}
