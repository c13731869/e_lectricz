package com.example.dao;

import com.example.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by admin on 14/03/2017.
 */
public interface CustomerDAO extends JpaRepository<Customer,Integer> {

    Customer findById(int id);

    Customer findByEmailAndPassword(String email, String password);

    List<Customer> findBySubscribed(boolean subscribed);
}
