package com.example.patterns.strategy;

/**
 * Created by admin on 24/03/2017.
 */
public class ApplyDiscountIMPL implements IApplyDiscountStrategy{
    @Override
    public double applyDiscount(double price) {
        return (price * .95);
    }

}
