package com.example.patterns.strategy;

/**
 * Created by admin on 24/03/2017.
 */
public interface IApplyDiscountStrategy {

    double applyDiscount(double price);
}
