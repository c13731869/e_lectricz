package com.example.patterns.observer;

/**
 * Created by admin on 27/03/2017.
 */
public abstract class Observer {

     Subject subject;
    public abstract void update();
}