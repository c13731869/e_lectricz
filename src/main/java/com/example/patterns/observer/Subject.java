package com.example.patterns.observer;

import com.example.entities.StockItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 27/03/2017.
 */
public class Subject {

    private List<Observer> observers = new ArrayList<Observer>();
    private StockItem item;

     StockItem getState() {

        return item;
    }

    public void setStock(StockItem item) {
        this.item = item;
        notifyAllObservers();
    }

     void attach(Observer observer){
        observers.add(observer);
    }

    private void notifyAllObservers(){
        for (Observer observer : observers) {
            observer.update();
        }
    }



}
