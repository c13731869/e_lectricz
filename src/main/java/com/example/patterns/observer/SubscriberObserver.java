package com.example.patterns.observer;

/**
 * Created by admin on 27/03/2017.
 */
import com.example.entities.Customer;
import com.example.patterns.observer.Observer;
import com.example.patterns.observer.Subject;
import com.example.services.impl.CustomerService;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SubscriberObserver extends Observer {


    public SubscriberObserver(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update()  {
        List<Customer> customerList = CustomerService.getInstance().getAllCustomers();

        for (Customer c: customerList) {
            if(c.isSubscribed()) {
                Properties mailServerProperties;
                Session getMailSession;
                MimeMessage generateMailMessage;

                InetAddress ip;
                String hostname = "";
                try {
                    ip = InetAddress.getLocalHost();
                    hostname = ip.getHostAddress();
                } catch (UnknownHostException e) {

                    e.printStackTrace();
                }

                mailServerProperties = System.getProperties();
                mailServerProperties.put("mail.smtp.port", "587");
                mailServerProperties.put("mail.smtp.auth", "true");
                mailServerProperties.put("mail.smtp.starttls.enable", "true");
                System.out.println("Mail Server Properties have been setup successfully..");

                System.out.println("\n\n 2nd ===> get Mail Session..");
                getMailSession = Session.getDefaultInstance(mailServerProperties, null);
                generateMailMessage = new MimeMessage(getMailSession);
                try {
                    generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(c.getEmail()));
                    generateMailMessage.setSubject("New item in stock");
                    String emailBody = "New " + subject.getState().getTitle() + " was added to our stock, you will recieve a 5% discount on the listed price. +" +
                            "the retail  price is "+subject.getState().getPrice()+" and there are "+subject.getState().getStockLevel() +" in stock.";
                    generateMailMessage.setContent(emailBody, "text/html");
                    System.out.println("Mail Session has been created successfully..");

                    System.out.println("\n\n 3rd ===> Get Session and Send mail");
                    Transport transport = getMailSession.getTransport("smtp");


                    transport.connect("smtp.gmail.com", "myprivatemessagebox@gmail.com", "(NOJ)!874!mp");
                    transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
                    transport.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}