package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ELectriczApplication {

	public static void main(String[] args) {
		SpringApplication.run(ELectriczApplication.class, args);
	}
}
