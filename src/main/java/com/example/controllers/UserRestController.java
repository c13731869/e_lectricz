package com.example.controllers;

import com.example.entities.Admin;
import com.example.entities.Customer;
import com.example.services.IAdminService;
import com.example.services.ICustomerService;
import com.example.services.impl.AdminService;
import com.example.services.impl.CustomerService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 17/03/2017.
 */
@RestController
@RequestMapping("/api/user")
public class UserRestController {

    ICustomerService iCustomerService;
    IAdminService iAdminService;

    @Autowired
    public void setiCustomerService(ICustomerService service){
        this.iCustomerService = service;
    }

//    @Autowired
//    public void setiAdminService(IAdminService service){
//
//        this.iAdminService=service;
//    }

    AdminService administratorService = AdminService.getInstance();
    CustomerService customerService = CustomerService.getInstance();

    @RequestMapping(value = "/getAllAdministrators", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Admin> getAllAdministrators() {

               return AdminService.getInstance().getAllAdmins();
    }

    @RequestMapping(value ="/allCustomers", method = RequestMethod.GET, produces = "application/json")
    public List<Customer> getAllCustomers(){
         //List<Customer>customers = iCustomerService.getAllCustomers();
        return CustomerService.getInstance().getAllCustomers();
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json")
    public Customer register(@RequestBody String jsonRegister){

        JSONObject jsonObject = new JSONObject(jsonRegister);
        String firstName = jsonObject.getString("firstName");
        String lastName = jsonObject.getString("lastName");
        String saddress1 = jsonObject.getString("saddress1");
        String saddress2 = jsonObject.getString("saddress2");
        String saddress3 = jsonObject.getString("saddress3");

        String paymentMethod = jsonObject.getString("paymentMethod");
        String email = jsonObject.getString("email");
        String password = jsonObject.getString("password");

        String confirmPassword = jsonObject.getString("confirmPassword");

        if(!password.equalsIgnoreCase(confirmPassword)){
            return null;
        }

        Customer aCustomer = new Customer();
        aCustomer.setFirstName(firstName);
        aCustomer.setLastName(lastName);

        String address = saddress1+" ,"+saddress2+" ,"+saddress3;
        aCustomer.setShippingAddress(address);
        aCustomer.setPaymentMethod(paymentMethod);
        aCustomer.setEmail(email);
        aCustomer.setPassword(password);
        aCustomer.setSubscribed(false);


        try{
            if(jsonObject.getBoolean("subscription")){
                aCustomer.setSubscribed(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        iCustomerService.saveCustomer(aCustomer);

        return aCustomer;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")
    public Customer login(@RequestBody String jsonLogin){

        JSONObject jsonObject = new JSONObject(jsonLogin);
        String email = jsonObject.getString("email");
        String password = jsonObject.getString("password");

        Customer cust = iCustomerService.findByEmailAndPassword(email,password);
        if(cust !=null){
            return cust;
        }else {
            return null;
        }
    }

    @RequestMapping(value = "/adminLogin", method = RequestMethod.POST, produces = "application/json")
    public Admin adminLogin(@RequestBody String jsonLogin){

        System.out.println("got into here");

        JSONObject jsonObject = new JSONObject(jsonLogin);
        String email = jsonObject.getString("email");
        String password = jsonObject.getString("password");

//        Admin admin = iAdminService.fingByEmailAndPassword(email,password);
        Admin admin = AdminService.getInstance().fingByEmailAndPassword(email, password);

        if(admin !=null){
            return admin;
        }else {
            return null;
        }
    }



    @RequestMapping(value ="/findCustomerByID", method = RequestMethod.POST, produces = "application/json")
    public Customer findCustomerById(@RequestBody String id){
        int idNO = Integer.parseInt(id);
        return iCustomerService.findById(idNO);
    }

    @RequestMapping(value ="/findCustomerByEmailAndPassword", method = RequestMethod.POST, produces = "application/json")
    public Customer findCustomerByEmailAndPassword(@RequestBody String email,String pass){
        return iCustomerService.findByEmailAndPassword(email,pass);
    }

    @RequestMapping(value ="/findAdminByID", method = RequestMethod.POST, produces = "application/json")
    public Admin findById(@RequestBody String id){
        int idNO = Integer.parseInt(id);
        return iAdminService.findById(idNO);
    }

    @RequestMapping(value ="/findAdminByEmailAndPassword", method = RequestMethod.POST, produces = "application/json")
    public Admin findAdminByEmailAndPassword(@RequestBody String email,String pass){
        return iAdminService.fingByEmailAndPassword(email,pass);
    }

    @RequestMapping(value ="/findSubscribed", method = RequestMethod.POST, produces = "application/json")
    public List<Customer> findBySubscribes(@RequestBody Customer cust){
        List<Customer> signedUpCustomers = new ArrayList<>();
        List<Customer> customers = getAllCustomers();
        for(Customer c : customers){
            if(c.isSubscribed()){
                signedUpCustomers.add(c);
            }
        }
        return signedUpCustomers;
    }


}
