package com.example.controllers;

import com.example.entities.Customer;
import com.example.entities.Purchase;
import com.example.entities.StockItem;
import com.example.patterns.strategy.ApplyDiscountIMPL;
import com.example.patterns.strategy.IApplyDiscountStrategy;
import com.example.services.ICustomerService;
import com.example.services.IPurchaseService;
import com.example.services.IStockItemService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by admin on 19/03/2017.
 */

@RestController
@RequestMapping("/api/purchase")
public class PurchaseRestController {

    IPurchaseService iPurchaseService;
    IStockItemService iStockItemService;
    ICustomerService iCustomerService;
    @Autowired
    public void setiCustomerService(ICustomerService service){
        this.iCustomerService=service;
    }
    @Autowired
    public void setiPurchaseService(IPurchaseService service){
        this.iPurchaseService=service;
    }
    @Autowired
    public void setiStockItemService(IStockItemService service){
        this.iStockItemService=service;
    }



    @RequestMapping(value ="/findPurchaseByID", method = RequestMethod.POST, produces = "application/json")
    public Purchase getPurchaseByID(@RequestBody String id){
        int idNO = Integer.parseInt(id);
        return iPurchaseService.findById(idNO);
    }

    @RequestMapping(value ="/findPurchaseByStockItem", method = RequestMethod.POST, produces = "application/json")
    public List<Purchase> findPurchaseByStockItem(@RequestBody String id){
        int idNO = Integer.parseInt(id);
        StockItem item = iStockItemService.findById(idNO);
        return  iPurchaseService.findByStockItem(item);
    }

    @RequestMapping(value ="/findPurchaseByCustomer", method = RequestMethod.POST, produces = "application/json")
    public List<Purchase> findPurchaseByCustomer(@RequestBody String id){
        return iPurchaseService.findByCustomer(iCustomerService.findById(Integer.parseInt(id)));
    }

    @RequestMapping(value ="/savePurchase", method = RequestMethod.POST, produces = "application/json")
    public void saveThisPurchase(@RequestBody String json){
        JSONObject jsonObject = new JSONObject(json);
        StockItem stockItem = iStockItemService.findById(Integer.parseInt(jsonObject.getString("itemID")));
        stockItem.setStockLevel(stockItem.getStockLevel()-1);
        Customer cust = iCustomerService.findById(Integer.parseInt( jsonObject.getString("customerID")));

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Purchase purchase = new Purchase();
        purchase.setCustomer(cust);
        purchase.setStockItem(stockItem);
        purchase.setPurchaseTime(timestamp);
        iPurchaseService.savePurchase(purchase);
    }



    @RequestMapping(value = "/deletePurchase",method = RequestMethod.POST, produces = "application/json")
    public void deleteUser(@RequestBody String id){
        iPurchaseService.deletePurchase(getPurchaseByID(id));
    }

//    @RequestMapping(value = "/openPurchases",method = RequestMethod.POST, produces = "application/json")
//    public List<Purchase> openPurchases(@RequestBody String id){
//
//        int idNO = Integer.parseInt(id);
//        Customer cust = iCustomerService.findById(idNO);
//
//        List<Purchase>open = new ArrayList<>();
//        for(Purchase p :iPurchaseService.findByCustomer(cust)){
//            if(!p.isPaid()){
//                open.add(p);
//            }
//        }
//        return  open;
//    }


    @RequestMapping(value = "/completePurchase",method = RequestMethod.POST, produces = "application/json")
    public void completePurchase(@RequestBody String json){


        JSONObject jsonObject = new JSONObject(json);
        String purchaseID = jsonObject.getString("purchaseID");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());


        Purchase purchase = getPurchaseByID(purchaseID);
        purchase.setPaymentTime(timestamp);
        purchase.setPaid(true);


        StockItem item = purchase.getStockItem();
        Customer customer = purchase.getCustomer();

        double priceToBePaid = 0;
        double discount = 0;
        double oldPrice = item.getPrice();

        if(customer.isSubscribed()){
            System.out.println("price before discount is "+item.getPrice());

            IApplyDiscountStrategy discountStrategy = new ApplyDiscountIMPL();
            double newPrice = discountStrategy.applyDiscount(oldPrice);
            System.out.println("new price is "+newPrice);

            priceToBePaid=newPrice;
            discount = oldPrice* .05;
        }else{
            System.out.println("price before discount is "+item.getPrice());

            priceToBePaid= oldPrice;
        }

        purchase.setDiscount(discount);
        purchase.setPricePaid(priceToBePaid);

        iPurchaseService.savePurchase(purchase);
    }

    @RequestMapping(value = "/findBasketTotal",method = RequestMethod.POST, produces = "application/json")
    public double findBasketTotal(@RequestBody String id){
        Customer cust = iCustomerService.findById(Integer.parseInt(id));
        double total = 0;
        List<Purchase> items = iPurchaseService.findByCustomer(cust);
        for(Purchase p : items){
            if(p.isPaid()==false){
                total+=p.getStockItem().getPrice();

            }
        }
        return  total;
    }

}
