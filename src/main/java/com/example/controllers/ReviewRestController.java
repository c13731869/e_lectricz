package com.example.controllers;

import com.example.entities.Customer;
import com.example.entities.Review;
import com.example.entities.StockItem;
import com.example.services.ICustomerService;
import com.example.services.IReviewService;
import com.example.services.IStockItemService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by admin on 19/03/2017.
 */
@RestController
@RequestMapping("/api/review")
public class ReviewRestController {

    IReviewService iReviewService;
    @Autowired
    public void setiReviewService(IReviewService service){
        this.iReviewService=service;
    }

    ICustomerService iCustomerService;
    @Autowired
    public void setiCustomerService(ICustomerService service){
        this.iCustomerService=service;
    }

    IStockItemService iStockItemService;
    @Autowired
    public void setiStockItemService(IStockItemService service){
        this.iStockItemService=service;
    }



    @RequestMapping(value ="/allReviews", method = RequestMethod.GET,produces = "application/json")
    @ResponseBody
    public List<Review> reviews(){
        return iReviewService.getAllReviews();
    }

    @RequestMapping(value ="/findReviewByID", method = RequestMethod.POST, produces = "application/json")
    public Review getReviewByID(@RequestBody String id){
        int idNO = Integer.parseInt(id);
        return iReviewService.findById(idNO);
    }

    @RequestMapping(value ="/findReviewByCustomer", method = RequestMethod.POST, produces = "application/json")
    public List<Review> getReviewByCustomerID(@RequestBody String id){
        int idNO = Integer.parseInt(id);
        Customer c = iCustomerService.findById(idNO);
        return iReviewService.findByCustomer(c);
    }

    @RequestMapping(value ="/findReviewByStockItem", method = RequestMethod.POST, produces = "application/json")
    public List<Review> getReviewByStockItem(@RequestBody String id){
        int idNO = Integer.parseInt(id);
        StockItem s = iStockItemService.findById(idNO);
        return  iReviewService.findByStockItem(s);
    }



    @RequestMapping(value ="/saveReview", method = RequestMethod.POST, produces = "application/json")
    public void saveThisPurchase(@RequestBody String json){

        JSONObject jsonObject = new JSONObject(json);

        String itemIDString = jsonObject.getString("itemID");

        String custIDString = jsonObject.getString("customerID");

        String review = jsonObject.getString("review");
        String starString = jsonObject.getString("stars");
        int stars = Integer.parseInt(starString);

        int itemIdNO = Integer.parseInt(itemIDString);
        int custIdNO = Integer.parseInt(custIDString);

        System.out.println("userId : "+custIDString+" itemID "+itemIDString);

        StockItem stockItem = iStockItemService.findById(itemIdNO);
        Customer cust = iCustomerService.findById(custIdNO);

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        Review aReview = new Review();
        aReview.setReview(review);
        aReview.setStars(stars);
        aReview.setCustomer(cust);
        aReview.setStockItem(stockItem);
        aReview.setPurchaseTime(timestamp);

        stockItem.setRating(getAverageReviews(stockItem));
        iStockItemService.saveStockItem(stockItem);
        iReviewService.saveReview(aReview);
    }



    public int getAverageReviews(StockItem stockItem){
        double totalStars =0;

        for(Review r : iReviewService.findByStockItem(stockItem)){
            totalStars+=r.getStars();
        }

        System.out.println("amount of reviews is : "+ iReviewService.findByStockItem(stockItem).size());
        int average = (int)Math.round(totalStars/iReviewService.findByStockItem(stockItem).size());
        return average;
    }
}
