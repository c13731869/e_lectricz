package com.example.controllers;

import com.example.entities.StockItem;
import com.example.patterns.observer.Subject;
import com.example.patterns.observer.SubscriberObserver;
import com.example.services.IStockItemService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 18/03/2017.
 */
@RestController
@RequestMapping("/api/stockItem")
public class StockItemRestController {

    IStockItemService iStockItemService;
    @Autowired
    public void setiStockItemService(IStockItemService service){
        this.iStockItemService=service;
    }

    @RequestMapping(value ="/findItemByID", method = RequestMethod.POST, produces = "application/json")
    public StockItem findById(@RequestBody String id){
        int idNO = Integer.parseInt(id);
        return iStockItemService.findById(idNO);
    }

    @RequestMapping(value ="/allStockItems", method = RequestMethod.GET, produces = "application/json")
    public List<StockItem> getAllStockItems(){
        System.out.println("all items is "+iStockItemService.getAllStockItems().size());
        return iStockItemService.getAllStockItems();
    }

    @RequestMapping(value ="/findByManufacturer", method = RequestMethod.GET, produces = "application/json")
    public List<StockItem> findByManufacturer(@RequestBody String manufacturer){
        List<StockItem>items = iStockItemService.findByManufacturer(manufacturer);
        return items;
    }

    @RequestMapping(value ="/findByPrice", method = RequestMethod.GET, produces = "application/json")
    public List<StockItem> findByPrice(@RequestBody String price){
        double priceDouble = Double.parseDouble(price);
        List<StockItem>items = iStockItemService.findByPrice(priceDouble);
        return items;
    }

    @RequestMapping(value ="/findByCategory", method = RequestMethod.GET, produces = "application/json")
    public List<StockItem> findByCategory(@RequestBody String category){
        List<StockItem>items = iStockItemService.findByCategory(category);
        return items;
    }

    @RequestMapping(value ="/findItemByTitle", method = RequestMethod.POST, produces = "application/json")
    public StockItem findByTitle(@RequestBody String title){
        return iStockItemService.findByTitle(title);
    }



    @RequestMapping(value ="/allPhones", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<StockItem> phones(){
        return iStockItemService.findByCategory("phone");
    }

    @RequestMapping(value ="/allTablets", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<StockItem> tablets(){
        return iStockItemService.findByCategory("tablet");
    }

    @RequestMapping(value ="/allLaptops", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<StockItem> laptops(){
        return iStockItemService.findByCategory("laptop");
    }

    @RequestMapping(value ="/allManufacturers", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<String> manufacturers(){
        return iStockItemService.allManufacturers();
    }



    @RequestMapping(value = "/stockUpdate", method = RequestMethod.POST, produces = "application/json")
    public StockItem register(@RequestBody String jsonRegister){

        System.out.println("got this "+jsonRegister);
        JSONObject jsonObject = new JSONObject(jsonRegister);
        String man = jsonObject.getString("manufacturer");
        String cat = jsonObject.getString("category");
        String tit = jsonObject.getString("title");
        String desc = jsonObject.getString("description");
        String img = jsonObject.getString("image");
        String c = jsonObject.getString("cost");
        double cost = Double.parseDouble(c);
        String q = jsonObject.getString("quantity");
        Integer qty = Integer.parseInt(q);


        StockItem si = new StockItem();
        si.setManufacturer(man);
        si.setCategory(cat);
        si.setTitle(tit);
        si.setDescription(desc);
        si.setImageUrl(img);
        si.setPrice(cost);
        si.setStockLevel(qty);

        try{
            iStockItemService.saveStockItem(si);
            Subject subject = new Subject();
            new SubscriberObserver(subject);
            subject.setStock(si);

            return si;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }


    @RequestMapping(value ="/takeOffSale", method = RequestMethod.POST, produces = "application/json")
    public StockItem takeOffSale(@RequestBody String id){

        StockItem off = iStockItemService.findById(Integer.parseInt(id));
        off.setOffSale(true);
        iStockItemService.saveStockItem(off);
        return off;
    }

    @RequestMapping(value ="/updateStockLevel", method = RequestMethod.POST, produces = "application/json")
    public void updateLevels(@RequestBody String updateParams){

        JSONObject jsonObject= new JSONObject(updateParams);
        StockItem itemToUpdate = iStockItemService.findById(Integer.parseInt(jsonObject.getString("id")));
        int updateBy = Integer.parseInt(jsonObject.getString("updateBy"));
        int oldStockLevel = itemToUpdate.getStockLevel();
        int newStockLevel = oldStockLevel + updateBy;
        itemToUpdate.setStockLevel(newStockLevel);
        iStockItemService.saveStockItem(itemToUpdate);

    }


}
