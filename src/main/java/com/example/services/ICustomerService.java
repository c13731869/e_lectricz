package com.example.services;

import com.example.entities.Customer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 16/03/2017.
 */
public interface ICustomerService {

    void saveCustomer(Customer customer);
    List<Customer> getAllCustomers();

    Customer findById(int id);

    Customer findByEmailAndPassword(String email, String password);

    List<Customer>findBySubscribed(boolean subscribed);
}
