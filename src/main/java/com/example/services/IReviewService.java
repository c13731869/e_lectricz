package com.example.services;

import com.example.entities.Customer;
import com.example.entities.Review;
import com.example.entities.StockItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 16/03/2017.
 */
public interface IReviewService {

    void saveReview(Review review);

    List<Review> getAllReviews();

    Review findById(int id);

    List<Review> findByCustomer(Customer customer);

    List<Review> findByStockItem(StockItem stockItem);


}
