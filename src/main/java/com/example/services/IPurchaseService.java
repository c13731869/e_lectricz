package com.example.services;


import com.example.entities.Customer;
import com.example.entities.Purchase;
import com.example.entities.StockItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 16/03/2017.
 */
public interface IPurchaseService {

    void savePurchase(Purchase purchase);

    List<Purchase> getAllPurchases();

    Purchase findById(int id);

    List<Purchase> findByStockItem(StockItem stockItem);

    List<Purchase> findByCustomer(Customer customer);


//    List<Purchase> openPurchases(Customer customer);

    void deletePurchase(Purchase purchase);


}
