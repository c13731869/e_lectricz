package com.example.services;

import com.example.entities.Admin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 16/03/2017.
 */
public interface IAdminService {

    void saveAdmin(Admin admin);

    List<Admin> getAllAdmins();

    Admin findById(int id);

    Admin fingByEmailAndPassword(String email, String password);



}
