package com.example.services;

import com.example.entities.StockItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 16/03/2017.
 */
public interface IStockItemService {

    void saveStockItem(StockItem stockItem);

    List<StockItem> getAllStockItems();

    StockItem findById(int i);
    StockItem findByTitle(String s);

    List<StockItem> findByManufacturer(String m);
    List<StockItem> findByPrice(double d);
    List<StockItem> findByCategory(String s);

    List<String> allManufacturers();

}
