package com.example.services.impl;

import com.example.dao.CustomerDAO;
import com.example.entities.Customer;
import com.example.services.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by admin on 16/03/2017.
 */
@Service
@Transactional
public class CustomerService implements ICustomerService {

    CustomerDAO customerDAO;

    private static AtomicReference<CustomerService> INSTANCE = new AtomicReference<CustomerService>();

    public CustomerService() {
        final CustomerService previous = INSTANCE.getAndSet(this);
        if(previous != null)
            throw new IllegalStateException("Second singleton " + this + " created after " + previous);
    }

    public static CustomerService getInstance() {
        return INSTANCE.get();
    }

    @Autowired
    public void setCustomerDAO(CustomerDAO dao){
        this.customerDAO=dao;
    }

    @Override
    public void saveCustomer(Customer customer) {
        customerDAO.save(customer);
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerDAO.findAll();
    }

    @Override
    public Customer findById(int id) {
        return customerDAO.findById(id);
    }

    @Override
    public Customer findByEmailAndPassword(String email, String password) {
        return customerDAO.findByEmailAndPassword(email,password);
    }

    @Override
    public List<Customer> findBySubscribed(boolean subscribed) {
        return customerDAO.findBySubscribed(subscribed);
    }
}
