package com.example.services.impl;

import com.example.dao.StockItemDAO;
import com.example.entities.StockItem;
import com.example.services.IStockItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 16/03/2017.
 */

@Service
@Transactional
public class StockItemService implements IStockItemService{

    private StockItemDAO stockItemDAO;

    @Autowired
    public void setStockItemDAO(StockItemDAO dao){
        this.stockItemDAO=dao;
    }


    @Override
    public void saveStockItem(StockItem stockItem) {
        stockItemDAO.save(stockItem);
    }

    @Override
    public List<StockItem> getAllStockItems() {

        return stockItemDAO.findAll();
    }

    @Override
    public StockItem findById(int i) {
        return stockItemDAO.findById(i);
    }

    @Override
    public StockItem findByTitle(String s) {
        return stockItemDAO.findByTitle(s);
    }

    @Override
    public List<StockItem> findByManufacturer(String m) {
        return stockItemDAO.findByManufacturer(m);
    }

    @Override
    public List<StockItem> findByPrice(double d) {
        return stockItemDAO.findByPrice(d);
    }

    @Override
    public List<StockItem> findByCategory(String s) {
        return stockItemDAO.findByCategory(s);
    }

    @Override
    public List<String> allManufacturers() {
        List<String> mans = new ArrayList<>();
        for(StockItem s : stockItemDAO.findAll()){
            mans.add(s.getManufacturer());
        }
        return mans;
    }
}
