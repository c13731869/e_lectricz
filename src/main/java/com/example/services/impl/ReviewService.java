package com.example.services.impl;

import com.example.dao.ReviewDAO;
import com.example.entities.Customer;
import com.example.entities.Review;
import com.example.entities.StockItem;
import com.example.services.IReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 16/03/2017.
 */
@Service
@Transactional
public class ReviewService implements IReviewService{

    private ReviewDAO reviewDAO;

    @Autowired
    public void setReviewDAO(ReviewDAO dao){
        this.reviewDAO=dao;
    }


    @Override
    public void saveReview(Review review) {
        reviewDAO.save(review);
    }

    @Override
    public List<Review> getAllReviews() {
        return reviewDAO.findAll();
    }

    @Override
    public Review findById(int id) {
        return reviewDAO.findById(id);
    }

    @Override
    public List<Review> findByCustomer(Customer customer) {
        return reviewDAO.findByCustomer(customer);
    }

    @Override
    public List<Review> findByStockItem(StockItem stockItem) {
        return reviewDAO.findByStockItem(stockItem);
    }
}
