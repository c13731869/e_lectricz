package com.example.services.impl;

import com.example.dao.AdminDAO;
import com.example.entities.Admin;
import com.example.services.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by admin on 16/03/2017.
 */

@Service
@Transactional
public class AdminService implements IAdminService {

    AdminDAO adminDAO;

    private static AtomicReference<AdminService> INSTANCE = new AtomicReference<AdminService>();

    public AdminService() {
        final AdminService previous = INSTANCE.getAndSet(this);
        if(previous != null)
            throw new IllegalStateException("Second singleton " + this + " created after " + previous);
    }

    public static AdminService getInstance() {
        return INSTANCE.get();
    }

    @Autowired
    public void setAdminDAO(AdminDAO dao){
        this.adminDAO = dao;
    }

    @Override
    public void saveAdmin(Admin admin) {
        adminDAO.save(admin);
    }

    @Override
    public List<Admin> getAllAdmins() {
        return adminDAO.findAll();
    }

    @Override
    public Admin findById(int id) {
        return adminDAO.findById(id);
    }

    @Override
    public Admin fingByEmailAndPassword(String email, String password) {
        return adminDAO.findAdminByEmailAndPassword(email,password);
    }
}
