package com.example.services.impl;

import com.example.dao.PurchaseDAO;
import com.example.entities.Customer;
import com.example.entities.Purchase;
import com.example.entities.StockItem;
import com.example.services.IPurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 16/03/2017.
 */
@Service
@Transactional
public class PurchaseService implements IPurchaseService{

    PurchaseDAO purchaseDAO;

    @Autowired
    public void setPurchaseDAO(PurchaseDAO dao){
        this.purchaseDAO=dao;
    }


    @Override
    public void savePurchase(Purchase purchase) {
        purchaseDAO.save(purchase);
    }

    @Override
    public List<Purchase> getAllPurchases() {
        return purchaseDAO.findAll();
    }

    @Override
    public Purchase findById(int id) {
        return purchaseDAO.findById(id);
    }

    @Override
    public List<Purchase> findByStockItem(StockItem stockItem) {
        return purchaseDAO.findByStockItem(stockItem);
    }

    @Override
    public List<Purchase> findByCustomer(Customer customer) {
        return purchaseDAO.findByCustomer(customer);
    }
//
//    @Override
//    public List<Purchase> openPurchases(Customer customer) {
//        return purchaseDAO.openPurchases(customer);
//    }

    @Override
    public void deletePurchase(Purchase purchase) {
        purchaseDAO.delete(purchase);
    }



}
