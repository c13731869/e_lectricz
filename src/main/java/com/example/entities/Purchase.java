package com.example.entities;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by admin on 16/03/2017.
 */
@Entity
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    private StockItem stockItem;

    @ManyToOne
    private Customer customer;

    private boolean paid;

    private Timestamp purchaseTime;
    private Timestamp paymentTime;

    private double discount;

    private double pricePaid;

    public Purchase() {
    }

    public Purchase(StockItem stockItem, Customer customer, boolean paid, Timestamp purchaseTime, Timestamp paymentTime,
                    double discount, double pricePaid) {
        this.stockItem = stockItem;
        this.customer = customer;
        this.paid = paid;
        this.purchaseTime = purchaseTime;
        this.paymentTime = paymentTime;
        this.discount = discount;
        this.pricePaid = pricePaid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StockItem getStockItem() {
        return stockItem;
    }

    public void setStockItem(StockItem stockItem) {
        this.stockItem = stockItem;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public Timestamp getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(Timestamp purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public Timestamp getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Timestamp paymentTime) {
        this.paymentTime = paymentTime;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getPricePaid() {
        return pricePaid;
    }

    public void setPricePaid(double pricePaid) {
        this.pricePaid = pricePaid;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "id=" + id +
                ", stockItem=" + stockItem +
                ", customer=" + customer +
                ", paid=" + paid +
                ", purchaseTime=" + purchaseTime +
                ", paymentTime=" + paymentTime +
                ", discount=" + discount +
                ", pricePaid=" + pricePaid +
                '}';
    }
}
