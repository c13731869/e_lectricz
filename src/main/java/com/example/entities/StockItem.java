package com.example.entities;

import javax.persistence.*;

/**
 * Created by admin on 14/03/2017.
 */
@Entity
public class StockItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String manufacturer;
    private String title;
    private String category;

    private String imageUrl;

    @Lob
    @Column(name="description",length = 9999)
    private String description;


    private double price;
    private int stockLevel;
    private int rating;

    private boolean offSale;

    public StockItem() {
    }

    public StockItem(String manufacturer, String title, String category, String imageUrl, String description,
                     double price, int stockLevel, int rating, boolean offSale) {
        this.manufacturer = manufacturer;
        this.title = title;
        this.category = category;
        this.imageUrl = imageUrl;
        this.description = description;
        this.price = price;
        this.stockLevel = stockLevel;
        this.rating = rating;
        this.offSale = offSale;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStockLevel() {
        return stockLevel;
    }

    public void setStockLevel(int stockLevel) {
        this.stockLevel = stockLevel;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public boolean isOffSale() {
        return offSale;
    }

    public void setOffSale(boolean offSale) {
        this.offSale = offSale;
    }

    @Override
    public String toString() {
        return "StockItem{" +
                "id=" + id +
                ", manufacturer='" + manufacturer + '\'' +
                ", title='" + title + '\'' +
                ", category='" + category + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", stockLevel=" + stockLevel +
                ", rating=" + rating +
                ", offSale=" + offSale +
                '}';
    }
}
