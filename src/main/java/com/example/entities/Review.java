package com.example.entities;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by admin on 16/03/2017.
 */

@Entity
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int stars;

    @ManyToOne
    private StockItem stockItem;

    @ManyToOne
    private Customer customer;

    @Lob
    @Column(name="review",length = 9999)
    private String review;

    private Timestamp purchaseTime;

    public Review() {
    }


    public Review(int stars, StockItem stockItem, Customer customer, String review, Timestamp purchaseTime) {
        this.stars = stars;
        this.stockItem = stockItem;
        this.customer = customer;
        this.review = review;
        this.purchaseTime = purchaseTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public StockItem getStockItem() {
        return stockItem;
    }

    public void setStockItem(StockItem stockItem) {
        this.stockItem = stockItem;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public Timestamp getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(Timestamp purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", stars=" + stars +
                ", stockItem=" + stockItem +
                ", customer=" + customer +
                ", review='" + review + '\'' +
                ", purchaseTime=" + purchaseTime +
                '}';
    }
}
