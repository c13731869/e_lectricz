angular.module('myApp',[
    'ngRoute',
    'ngSanitize',
    'ngAnimate',
    'ngAria',
    'ngMaterial',
    'ngCookies',
    'chart.js',
    'ui.router',
    'myApp.LogRegController',
    'myApp.HomeController',
    'myApp.AdminLoginController',
    'myApp.PhoneCategoryController',
    'myApp.TabletCategoryController',
    'myApp.LaptopCategoryController',
    'myApp.ItemDescriptionController',
    'myApp.navbar',
    'myApp.ViewItemsController',
    'myApp.ViewCustomersController',
    'myApp.AddNewItemController',
    'myApp.BasketController',
    'myApp.PurchaseHistoryController',
]).
config(function ($stateProvider,$urlRouterProvider) {

    $urlRouterProvider.otherwise("/home");

    $stateProvider
        .state('home',{
            url:"/home",
            templateUrl: "resources/js/views/Home.html",
            controller:"HomeController"
        })
        .state('logreg',{
            url: '/logreg',
            templateUrl: "resources/js/views/LogReg.html",
            controller: "LogRegController"
        })
        .state('adminLogin',{
            url: '/adminLogin',
            templateUrl: "resources/js/views/AdminLogin.html",
            controller: "AdminLoginController"
        })
        .state('phoneCategory',{
            url: '/phoneCategory',
            templateUrl: "resources/js/views/PhoneCategory.html",
            controller: "PhoneCategoryController"
        })
        .state('tabletCategory',{
            url: '/tabletCategory',
            templateUrl: "resources/js/views/TabletCategory.html",
            controller: "TabletCategoryController"
        })
        .state('laptopCategory',{
            url: '/laptopCategory',
            templateUrl: "resources/js/views/LaptopCategory.html",
            controller: "LaptopCategoryController"
        })
        .state('itemDescription',{
            url: '/itemDescription/{stockItemID}',
            templateUrl: "resources/js/views/ItemDescription.html",
            controller: "ItemDescriptionController"
        })
        .state('viewItems',{
            url: '/viewItems',
            templateUrl: "resources/js/views/ViewItems.html",
            controller: "ViewItemsController"
        })
        .state('viewCustomers',{
            url: '/viewCustomers',
            templateUrl: "resources/js/views/ViewCustomers.html",
            controller: "ViewCustomersController"
        })
        .state('addNewItem',{
            url: '/addNewItem',
            templateUrl: "resources/js/views/AddNewItem.html",
            controller: "AddNewItemController"
        })
        .state('basket',{
            url: '/basket',
            templateUrl: "resources/js/views/Basket.html",
            controller: "BasketController",
            params: { obj: null }
        })
        .state('purchaseHistory',{
            url: '/purchaseHistory/{custID}',
            templateUrl: "resources/js/views/PurchaseHistory.html",
            controller: "PurchaseHistoryController",
            params: { obj: null }
        });
});

