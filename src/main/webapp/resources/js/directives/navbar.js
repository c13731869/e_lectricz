angular.module('myApp.navbar',[]).
directive('navbar', function($cookieStore, $state, $http, $interval){
    return{
        restrict:'E',
        scope : {},
        controller: function ($scope, $rootScope) {


            $rootScope.userLoggedOn = false;
            $rootScope.adminLoggedOn = false;


            if($cookieStore.get('userCookie')){
                $rootScope.userLoggedOn = true;
                $scope.currentUser = $cookieStore.get('userCookie');
            }


            if($cookieStore.get('adminCookie')){
                $rootScope.adminLoggedOn = true;
            }



            $scope.remove=function(x){
                $http.post('/api/purchase/deletePurchase', x.id)
                    .success(function (data, status) {
                        if(status = 200){
                            $rootScope.updateCart();

                        }
                    }).error(function (error) {
                    console.log("something went wrong in /findPurchaseByCustomer!!");
                });
            };

            $scope.purchase = function(x){
                $http.post('/api/purchase/completePurchase', x.id)
                    .success(function (data, status) {

                        if(status = 200){

                            $scope.purchases = data;
                        }
                    }).error(function (error) {
                    console.log("something went wrong in /findPurchaseByCustomer!!");
                });
            };

            $scope.shoppingCartInterval = function () {
                $scope.promise = $interval(function () {
                    $rootScope.updateCart();


                }, 3000);
            } ;

            $scope.shoppingCartInterval();
            $rootScope.updateCart = function () {
                if($rootScope.userLoggedOn) {
                    $http.post('/api/purchase/findPurchaseByCustomer', $scope.currentUser.id)
                        .success(function (data, status) {

                            $rootScope.pp=0;
                            if (status = 200) {
                                $scope.purchases = data;
                                for(i=0;i<$scope.purchases.length;i++){
                                    if($scope.purchases[i].paid==false){
                                        $rootScope.pp++;
                                    }
                                }
                            }
                        }).error(function (error) {
                        console.log("something went wrong in /findPurchaseByCustomer!!");
                    });

                    $http.post('/api/purchase/findBasketTotal', $scope.currentUser.id)
                        .success(function (data, status) {

                            if (status = 200) {
                                $scope.total = data;
                                console.log(data, "findBasketTotal");
                            }
                        }).error(function (error) {
                        console.log("something went wrong in /findBasketTotal!!");
                    });


                }
            };
            $rootScope.updateCart();

            // $http.post('/api/purchase/openPurchases', $scope.currentUser.id)
            //     .success(function (data, status) {
            //
            //         if(status = 200){
            //             $scope.openPurchases = data;
            //         }
            //     }).error(function (error) {
            //     console.log("something went wrong in /openPurchases!!");
            // });

            $scope.moveToCheckoutArea = function(){
                $scope.checkoutObj = {};
                $scope.checkoutObj.customerId = $scope.currentUser.id;
                $scope.checkoutObj.basketItems = $scope.purchases;
                $scope.checkoutObj.total = $scope.total;

                $state.go('basket',{obj:$scope.checkoutObj});
            }
            // $scope.beginCheckout = function(){
            //     // $scope.checkoutObj = {};
            //     // $scope.checkoutObj.customerId = $scope.currentUser.id;
            //     // $scope.checkoutObj.basketItems = $scope.purchases;
            //     //
            //     // $state.go('checkout',{obj:$scope.checkoutObj});
            // };

            $scope.logOutUser = function () {
                $cookieStore.remove('userCookie');
                $rootScope.userLoggedOn = false;
                $state.go('home');
            };

            $scope.logOutAdmin = function () {
                $cookieStore.remove('adminCookie');
                $rootScope.adminLoggedOn = false;
                $state.go('home');

            };

            
        },
        templateUrl:'resources/js/directives/navbar.html',
        replace:true
    }
});