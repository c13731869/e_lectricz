angular.module('myApp.LaptopCategoryController',['myApp.IteratorService','chart.js']).
controller('LaptopCategoryController',function($scope,$http,$state,$cookieStore,$rootScope,IteratorService) {





    $scope.currUser = $cookieStore.get('userCookie');

    $scope.orderTypes = ["Price","Title","Manufacturer"];

    $scope.init = function(){
        $http.get('/api/stockItem/allLaptops')
            .success(function (data, status) {
                if(status = 200){
                    $scope.allLaptops = [];
                    console.log("all laptops ",$scope.allLaptops);

                    $scope.labels=[];
                    $scope.data = [];

                    var iter = new IteratorService.Iterator(data);
                    for (var item = iter.first(); iter.hasNext(); item = iter.next()) {
                        $scope.allLaptops.push(item);


                        $scope.labels.push(item.title);
                        $scope.data.push(item.rating);
                    }

                    $scope.chartIt();

                }
            }).error(function (error) {
            console.log("something went wrong in allLaptops !!");
        });
    }
    $scope.init();

    $scope.chartIt = function(){

        // $scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
        // $scope.data = [300, 500, 100,400,200];
        console.log("labels are ",$scope.labels);
    }

    $scope.fullDescription = function(x){

        console.log(x);

        $state.go('itemDescription',{stockItemID:x});
    };





    $scope.basketAdd = function(x){
        $scope.purhcaseParams = {};
        $scope.purhcaseParams.customerID = $scope.currUser.id+"";
        $scope.purhcaseParams.itemID = x.id+"";
        $http.post('/api/purchase/savePurchase',JSON.stringify($scope.purhcaseParams))
            .success(function (data, status) {
                if(status == 200) {
                    swal(x.title, "successfully added to basket", "success");
                    $rootScope.updateCart();
                }
            })
            .error(function (error) {
                alert("Basket error");
            });
    };

});

