angular.module('myApp.PurchaseHistoryController',[]).
controller('PurchaseHistoryController',function($scope,$http,$state,$cookieStore,$rootScope,$stateParams, $interval) {
    $scope.currUser = $cookieStore.get('userCookie');

    var custId = $stateParams.custID+"";
    console.log("in pur con with ",custId);
    $scope.ph="worked";

    $scope.getCustomerPurchases=function(){

        console.log("inside $scope.getCustomerPurchases= with ",custId);

        $http.post('/api/purchase/findPurchaseByCustomer',custId)
            .success(function (data, status) {
                if(status == 200) {
                    $scope.customerPurchases = data;
                    console.log($scope.customerPurchases);
                }
            })
            .error(function (error) {
                console.log("View Customer purchases error");
            });
    };
    $scope.getCustomerPurchases();
});