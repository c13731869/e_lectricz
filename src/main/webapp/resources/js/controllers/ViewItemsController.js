angular.module('myApp.ViewItemsController',[]).
controller('ViewItemsController',function($scope,$http,$state,$cookieStore,$rootScope,$stateParams) {
    $scope.currUser = $cookieStore.get('userCookie');


    $scope.updatedQTY=0;
    $scope.updateStockDatabase = function(x,qty){
        console.log("updating ",x, "by ",$scope.updatedQTY," units");

            var item = {};
            item.id = x.id+"";
            item.updateBy=qty+"";

            $http.post('/api/stockItem/updateStockLevel',JSON.stringify(item))
                .success(function (data, status) {
                    if(status == 200) {
                        $scope.viewItems();

                        swal(x.title ,"Stock Level Adjusted", "success");
                    }
                })
                .error(function (error) {
                    console.log("View stock items error");
                });

    };


    $scope.viewItems = function(){
        $http.get('/api/stockItem/allStockItems')
            .success(function (data, status) {
                if(status == 200) {
                    $scope.allItems = data;
                    console.log("all items ",data);
                    swal("List of all stock items retrieved", "Choose Option", "success");
                }
            })
            .error(function (error) {
                console.log("View stock items error");
            });
    };
    $scope.viewItems();

    $scope.tit="";

    $scope.delete = function(x){
        var i = x.id+"";
        console.log("sending ",i);
        $http.post('/api/stockItem/takeOffSale',i)
            .success(function (data, status) {
                if(status == 200) {
                    console.log("too off  ",data.title);
                    swal(x.title, "TAKEN OFF SALE","error");
                }
            })
            .error(function (error) {
                console.log("Take off sale error");
            });
    };

});