angular.module('myApp.PhoneCategoryController',['ngMaterial', 'jkAngularRatingStars']).
controller('PhoneCategoryController',function($scope,$http,$state,$cookieStore,$rootScope) {

    $scope.currUser = $cookieStore.get('userCookie');



    $scope.init = function(){
        $http.get('/api/stockItem/allPhones')
            .success(function (data, status) {
                if(status = 200){
                    $scope.allPhones = data;
                }
            }).error(function (error) {
            console.log("something went wrong in allPhones !!");
        });
    }
    $scope.init();


    $scope.fullDescription = function(x){

        console.log(x);

        $state.go('itemDescription',{stockItemID:x});
    };


    $scope.basketAdd = function(x){
        $scope.purhcaseParams = {};
        $scope.purhcaseParams.customerID = $scope.currUser.id+"";
        $scope.purhcaseParams.itemID = x.id+"";
        $http.post('/api/purchase/savePurchase',JSON.stringify($scope.purhcaseParams))
            .success(function (data, status) {
                if(status == 200) {
                    $rootScope.updateCart();
                    swal(x.title, "successfully added to basket", "success");
                }
            })
            .error(function (error) {
                alert("Basket error");
            });


    };

});

