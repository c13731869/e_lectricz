angular.module('myApp.BasketController',[]).
controller('BasketController',function($scope,$http,$state,$cookieStore,$rootScope,$stateParams, $interval) {

    if($cookieStore.get('userCookie')){
        $rootScope.userLoggedOn = true;
        $scope.currentUser = $cookieStore.get('userCookie');
    }

    $scope.QTY = 1;

    var checkoutParams = $stateParams.obj;
    $scope.purchases = checkoutParams.basketItems;
    $scope.custId = checkoutParams.customerId;
    $scope.total = checkoutParams.total;

    if($scope.currentUser.subscribed){
        $scope.disc = $scope.total * .05;
    }else{
        $scope.disc = 0;
    }
    $scope.discountView = $scope.disc.toFixed(2);

    var pur = checkoutParams.basketItems;


    $scope.remove=function(x){
        console.log("removed ",x.stockItem.title);
        var i = x.title;
        $http.post('/api/purchase/deletePurchase', x.id)
            .success(function (data, status) {
                if(status = 200){
                    swal(x.stockItem.title, "successfully removed from basket", "error");

                    $rootScope.pp--;

                    $rootScope.updateCart();
                }
            }).error(function (error) {
            console.log("something went wrong in /findPurchaseByCustomer!!");
        });
    };


    $scope.pay= function(){


        var values = $scope.purchases;
        angular.forEach(values, function(value, key){
            // console.log(key + ': ' + value);
            console.log("value is ",value);
            console.log("pur id ",value.id);
            console.log("val id ",value.stockItem.id);

            $scope.params ={};
            $scope.params.purchaseID = value.id+"";



            // console.log(key);
            $http.post('/api/purchase/completePurchase', JSON.stringify($scope.params))
                .success(function (data, status) {

                    if(status = 200){

                        $scope.purchases = data;
                        swal("Purchase Completed","come again soon","success");
                        $rootScope.pp=0;
                        $scope.total=0;
                        // $state.go('home');


                    }
                }).error(function (error) {
                console.log("something went wrong in /findPurchaseByCustomer!!");
            });
        });
    };


    // $scope.purchase = function(x){
    //     $http.post('/api/purchase/completePurchase', x.id)
    //         .success(function (data, status) {
    //
    //             if(status = 200){
    //
    //                 // $scope.purchases = data;
    //             }
    //         }).error(function (error) {
    //         console.log("something went wrong in /findPurchaseByCustomer!!");
    //     });
    // };

    $scope.shoppingCartInterval = function () {
        $scope.promise = $interval(function () {
            $rootScope.updateCart();
        }, 8000);
    } ;

    $scope.shoppingCartInterval();

    $rootScope.updateCart = function () {
        if($rootScope.userLoggedOn) {
            $http.post('/api/purchase/findPurchaseByCustomer', $scope.currentUser.id)
                .success(function (data, status) {

                    if (status = 200) {
                        $scope.purchases = data;
                    }
                }).error(function (error) {
                console.log("something went wrong in /findPurchaseByCustomer!!");
            });

            $http.post('/api/purchase/findBasketTotal', $scope.currentUser.id)
                .success(function (data, status) {

                    if (status = 200) {
                        $scope.total = data;
                        console.log(data, "findBasketTotal");
                    }
                }).error(function (error) {
                console.log("something went wrong in /findBasketTotal!!");
            });
        }
    };
    $rootScope.updateCart();


});