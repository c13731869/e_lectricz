angular.module('myApp.TabletCategoryController',[]).
controller('TabletCategoryController',function($scope,$http,$state,$cookieStore,$rootScope,$stateParams) {

    $scope.currUser = $cookieStore.get('userCookie');


    $scope.fullDescription = function(x){

        console.log(x);

        $state.go('itemDescription',{stockItemID:x});
    };

    $scope.init = function(){
        $http.get('/api/stockItem/allTablets')
            .success(function (data, status) {
                if(status = 200){
                    $scope.allTablets = data;
                }
            }).error(function (error) {
            console.log("something went wrong in allTablets !!");
        });
    };
    $scope.init();

    $scope.basketAdd = function(x){
        $scope.purhcaseParams = {};
        $scope.purhcaseParams.customerID = $scope.currUser.id+"";
        $scope.purhcaseParams.itemID = x.id+"";
        $http.post('/api/purchase/savePurchase',JSON.stringify($scope.purhcaseParams))
            .success(function (data, status) {
                if(status == 200) {
                    $rootScope.updateCart();
                    swal(x.title, "successfully added to basket", "success");
                }
            })
            .error(function (error) {
                alert("Basket error");
            });
    };
});

