angular.module('myApp.ViewCustomersController',[]).
controller('ViewCustomersController',function($scope,$http,$state,$cookieStore,$rootScope,$stateParams) {

    $scope.currUser = $cookieStore.get('userCookie');


    $scope.viewCustomers = function(){


        $http.get('/api/user/allCustomers')
            .success(function (data, status) {
                if(status == 200) {
                    $scope.allCustomers = data;
                    swal("List of all customers retrieved", "Choose Option", "success");
                }
            })
            .error(function (error) {
                console.log("View Customers error");
            });
    };
    $scope.viewCustomers();

    $scope.purchaseHistory = function(x){
        console.log(x.id);
        $state.go('purchaseHistory',{custID:x.id})
    }
});

