angular.module('myApp.ItemDescriptionController',['ngMaterial', 'jkAngularRatingStars']).
controller('ItemDescriptionController',function($scope,$http,$state,$cookieStore,$rootScope,$stateParams) {

    $scope.currUser = $cookieStore.get('userCookie');


    $scope.itemID = $stateParams.stockItemID;
    console.log($scope.itemID);
    $scope.itemDescriptionMessage = $scope.itemID;

    $scope.firstRate = 0;
    $scope.firstRateByUser = 0;
    $scope.secondRate = 3;
    $scope.readOnly = true;



    $scope.submitReview = function(){
        $scope.reviewParams = {};
        $scope.reviewParams.customerID = $scope.currUser.id+"";
        $scope.reviewParams.review = $scope.userReview;
        $scope.reviewParams.itemID = $scope.thisItem.id+"";
        $scope.reviewParams.stars = $scope.firstRateByUser+"";

        $http.post('/api/review/saveReview', JSON.stringify($scope.reviewParams))
            .success(function (data, status) {
                if(status = 200){

                    swal($scope.thisItem.title, " review successfully posted", "success");

                }
            }).error(function (error) {
            console.log("something went wrong!!");
        });

    };

    $scope.findReviews = function(){
        $http.post('/api/review/findReviewByStockItem', $scope.itemID)
            .success(function (data, status) {
                if(status = 200){

                    $scope.thisItemReviews = data;

                }
            }).error(function (error) {
            console.log("something went wrong!!");
        });
    }


    $scope.getThisItem = function(){
        $http.post('/api/stockItem/findItemByID', $scope.itemID)
            .success(function (data, status) {
                if(status = 200){

                   $scope.thisItem = data;

                }
            }).error(function (error) {
            console.log("something went wrong!!");
        });

        $scope.findReviews();
    };
    $scope.getThisItem();


    $scope.basketAdd = function(x){
        console.log($scope.currUser);

        console.log("currUser ",$scope.currUser.id);
        console.log("item ",x);

        $scope.purhcaseParams = {};
        $scope.purhcaseParams.customerID = $scope.currUser.id+"";
        $scope.purhcaseParams.itemID = x+"";
        $http.post('/api/purchase/savePurchase',JSON.stringify($scope.purhcaseParams));
    }



});

