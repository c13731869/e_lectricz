angular.module('myApp.LogRegController',[]).
controller('LogRegController',function($scope,$http,$state,$cookieStore,$rootScope) {
    $(function () {

        $('#login-form-link').click(function (e) {
            $("#login-form").delay(100).fadeIn(100);
            $("#register-form").fadeOut(100);
            $('#register-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
        $('#register-form-link').click(function (e) {
            $("#register-form").delay(100).fadeIn(100);
            $("#login-form").fadeOut(100);
            $('#login-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });

    });

    $scope.submitRegistration =function(){

        console.log($scope.register);

        $http.post('/api/user/register', JSON.stringify($scope.register))
            .success(function (data, status) {
                if(status = 200){

                    $scope.register = data;
                    $state.go('home');
                    $cookieStore.put('userCookie',$scope.register);

                    console.log(data.firstName+" registered successfully!!");
                }
            }).error(function (error) {
            console.log("something went wrong!!");
        });
    };


    $scope.submitLogin=function(){

        $http.post('/api/user/login', JSON.stringify($scope.login))
            .success(function (data, status) {
                if(status = 200){

                    $scope.login = data;
                    $rootScope.userLoggedOn = true;

                    console.log($scope.login.firstName," successfully logged in")

                    $state.go('home');

                    $rootScope.userName = $scope.login.firstName;

                    $cookieStore.put('userCookie',$scope.login);


                }
            }).error(function (error) {
            console.log("something went wrong!!");
        });
    };


});

