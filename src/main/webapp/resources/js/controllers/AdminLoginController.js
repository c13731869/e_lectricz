angular.module('myApp.AdminLoginController',[]).
controller('AdminLoginController',function($scope,$http,$state,$cookieStore,$rootScope) {



    $scope.adminLoginRun=function(){

        console.log("got into adminLogin()");

        $http.post('/api/user/adminLogin', JSON.stringify($scope.adminLogin))
            .success(function (data, status) {
                if(status = 200){

                    $scope.adminLogin = data;
                    $cookieStore.put('adminCookie',$scope.adminLogin);
                    $rootScope.adminLoggedOn = true;

                    $rootScope.adminName = $scope.adminLogin.firstName;

                    console.log($scope.adminLogin," " ,$rootScope.adminLoggedOn);

                    $state.go('home');
                }
            }).error(function (error) {
            console.log("something went wrong in adminLogin !!");
        });
    };

});